# Název Aplikace

## Seznam Autorů
- Jan Novák
- Markéta Svobodová
- Petr Černý

## Seznam Použitých Technologií
- Node.js
- Express.js
- MongoDB
- React.js
- Redux
- Sass

## Popis
Tato aplikace je jednoduchý blogovací systém, který umožňuje uživatelům vytvářet, číst a komentovat články. Uživatelé se mohou také registrovat a přihlašovat se do svých účtů.

## Název Instalace
1. Nejdříve si zkopírujte repozitář pomocí příkazu `git clone https://example.com/repo.git`.
2. Přejděte do složky s projektem pomocí příkazu `cd nazev-aplikace`.
3. Nainstalujte závislosti pomocí příkazu `npm install`.
4. Spusťte aplikaci pomocí příkazu `npm start`.

## Copyright
© 2024 Název Aplikace. Všechna práva vyhrazena.

